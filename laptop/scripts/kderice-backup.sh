#!/bin/bash
set -euo pipefail
set -x

cp -r $HOME/.config/kitty $HOME/$SCRIPTHOME/configs/.config/kitty
konsave -s kde
konsave -e kde