#! /usr/bin/zsh

set -e
set -x

# args=("$@")

setopt nocorrect
pacman -Sy --noconfirm git
ssh-keyscan -H github.com >> ~/.ssh/known_hosts
# git clone git@github.com:BPplays/arch-auto-installs.git
git clone https://gitlab.com/BPplays/arch_auto_installs.git
# cd ./arch_auto_installs/laptop
chmod -R +x ./arch_auto_installs
# ./aui.sh
# ./arch_auto_installs/laptop/aui.sh "${args[@]}"
./arch_auto_installs/laptop/aui.sh --skip-mirror-check